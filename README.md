###CALCULATOR MS

Esta primera versión solo hace la operación básica ADD
Aparte de esto en la siguiente versión se usara ReactiveMongoRepository 



Para levantar la application es necesario tener docker



Mongo
En la carpeta que descargemos el fichero de src/resources/docker-compose.yml
ejecutar 
```
  docker-compose up -docker
```


Rabbit 
```
  docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

##Instalacion

Modulo Orchestrator
```
  mvn clean install
  mvn spring-boot:run
```

Modulo Addition
```
  mvn clean install
  mvn spring-boot:run

  ```




### Curl para crear operaciones 

Más ejemplpos en src/resources

```
curl -X POST \
  http://localhost:8080/operation \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: bbed84c9-96de-420f-b360-9fa7f0fdbdd3' \
  -H 'cache-control: no-cache' \
  -d '{
   "operator1":{
      "operator1":null,
      "operator2":null,
      "operation":null,
      "value":5
   },
   "operator2":{
      "operator1":{
         "operator1":null,
         "operator2":null,
         "operation":null,
         "value":10
      },
      "operator2":{
         "operator1":null,
         "operator2":null,
         "operation":null,
         "value":3
      },
      "operation":"SUB",
      "value":7
   },
   "operation":"ADD",
   "value":12
}'

```


### Curl para obtener operaciones

```

curl -X GET \
  http://localhost:8080/operation/226 \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: bc22203b-ceeb-41fd-adca-6e926ecc5657' \
  -H 'cache-control: no-cache'


  ```


