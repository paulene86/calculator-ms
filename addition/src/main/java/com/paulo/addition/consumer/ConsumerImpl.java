package com.paulo.addition.consumer;

import com.paulo.addition.repository.OperatorRepository;
import com.paulo.model.Operator;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerImpl implements Consumer<Operator> {

  private static final String QUEUE_ADDITION = "QUEUE_ADDITION";

  private final OperatorRepository operatorRepository;

  public ConsumerImpl(final OperatorRepository operatorRepository) {
    this.operatorRepository = operatorRepository;
  }


  @Override
  @RabbitListener(queues = QUEUE_ADDITION)
  public void consume(Operator operator) {
    System.out.println("[XXX] Consuming operator addition");
    //operator.setValue(operator.getOperator1().getValue() + operator.getOperator2().getValue());
    operatorRepository.save(operator);
    System.out.println("[X] Save from consumer with id : " + operator.getOperatorId());

  }
}
