package com.paulo.addition.consumer;



public interface Consumer<T> {

  void consume(T t);

}
