package com.paulo.model;

import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class Operator implements Serializable {

  @Id
  private Long operatorId;
  private Operator operator1;
  private Operator operator2;
  private Operation operation;
  private Double value;
  private Long parenId;
}
