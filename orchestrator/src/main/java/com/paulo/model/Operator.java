package com.paulo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Operator implements Serializable {

    @Id
    private Long operatorId;
    private Operator operator1;
    private Operator operator2;
    private Operation operation;
    private Double value;
    private Long parenId;

    @JsonIgnore
    @JsonProperty(value = "operatorId")
    public Long getOperatorId() {
        return operatorId;
    }
}
