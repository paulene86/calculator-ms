package com.paulo.rest.message.producer;

import com.paulo.model.Operator;

public interface Producer {

    void publishMessage(Operator operator);

}
