package com.paulo.rest.message.producer;


import com.paulo.model.Operator;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ProducerImpl implements Producer {

    public final static String QUEUE_NAME = "QUEUE_ADDITION";

    @Bean
    public Queue myQueue() {
        return new Queue(QUEUE_NAME, false);
    }

    private RabbitTemplate rabbitTemplate;

    public ProducerImpl(final RabbitTemplate rabbitTemplate){
        this.rabbitTemplate=rabbitTemplate;
    }

    @Override
    public void publishMessage(Operator operator) {
        rabbitTemplate.convertAndSend(QUEUE_NAME, operator);
        System.out.println("[X] Message with operator");
    }

}
