package com.paulo.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {
    "com.paulo.rest.repository", "com.paulo.rest.controller", "com.paulo.rest.service",
    "com.paulo.rest.message.producer"})
@EnableMongoRepositories
public class OrchestratorApplication {

  public static void main(String[] args) {
    SpringApplication.run(OrchestratorApplication.class, args);
  }
}
