package com.paulo.rest.service;

import com.paulo.model.Operator;
import com.paulo.rest.message.producer.Producer;
import com.paulo.rest.repository.OperatorRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    private final Producer producer;

    private final OperatorRepository operatorRepository;


    @Autowired
    public CalculatorServiceImpl(final Producer producer,
        final OperatorRepository responseRepository) {
        this.producer = producer;
        this.operatorRepository = responseRepository;
    }

    @Override
    public Long performOperation(Operator operator) {

        final long idOperator = 1L + (long) (Math.random() * (1000L - 1L));
        operator.setOperatorId(idOperator);

        operatorRepository.save(operator);

        if (!isAlreadyResolved(operator)) {
            List<Operator> operatorList = new ArrayList<>();
            decomposeOperator(idOperator, operator, operatorList);

            /**
             operatorList.forEach(op -> {
             System.out.println(op.toString());
             producer.publishMessage(op);
             });
             **/

        }

        publishOperator(operator);

        return operator.getOperatorId();
    }

    @Override
    public Operator obtainOperation(Long processId) {
        final Operator operator = operatorRepository.findById(processId).orElse(null);
        if (operator.getValue() == null) {
            operator.setValue(getValueOperator(operator));
            operatorRepository.save(operator);
        }
        return operator;

    }

    private Double getValueOperator(Operator operator) {

        if (operator.getOperator1() == null && operator.getOperator2() == null) {
            return operator.getValue();
        } else {
            Double valueOperator1 = getValueOperator(operator.getOperator1());
            Double valueOperator2 = getValueOperator(operator.getOperator2());
            switch (operator.getOperation()) {
                case ADD:
                    return valueOperator1 + valueOperator2;
                case SUB:
                    return valueOperator1 - valueOperator2;
                case MUL:
                    return valueOperator1 * valueOperator2;
                case DIV:
                    return valueOperator1 / valueOperator2;
                default:
                    return null;
            }
        }
    }


    private boolean isAlreadyResolved(Operator operator) {
        return operator.getOperator1() == null
            && operator.getOperator2() == null
            && operator.getOperation() == null;
    }

    private void decomposeOperator(Long parentId, Operator operator, List<Operator> listOperator) {
        if (operator.getOperator1() == null && operator.getOperator2() == null
            && operator.getOperation() == null) {
            //( god is his own parent :)
            //operator.setParenId(parentId);
            listOperator.add(operator);
        } else {
            if (operator.getOperator1() != null) {
                final long idNewOperator = 1L + (long) (Math.random() * (1000L - 1L));
                operator.getOperator1().setOperatorId(idNewOperator);
                operator.getOperator1().setParenId(parentId);
                decomposeOperator(idNewOperator, operator.getOperator1(), listOperator);
            }
            if (operator.getOperator2() != null) {
                final long idNewOperator = 1L + (long) (Math.random() * (1000L - 1L));
                operator.getOperator2().setOperatorId(idNewOperator);
                operator.getOperator2().setParenId(parentId);
                decomposeOperator(idNewOperator, operator.getOperator2(), listOperator);
            }
        }
    }

    private void publishOperator(Operator operator) {

        producer.publishMessage(operator);

        if (operator.getOperator1() != null && operator.getOperator2() != null
            && operator.getOperation() != null) {
            System.out.println(operator.toString());
            publishOperator(operator.getOperator1());
            publishOperator(operator.getOperator2());
        }

    }


}
