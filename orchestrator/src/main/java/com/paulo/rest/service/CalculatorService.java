package com.paulo.rest.service;

import com.paulo.model.Operator;

public interface CalculatorService {

    Long performOperation(Operator operator);

    Operator obtainOperation(Long processId);

}
