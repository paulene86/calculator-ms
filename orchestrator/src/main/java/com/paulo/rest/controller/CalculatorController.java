package com.paulo.rest.controller;

import com.paulo.model.Operator;
import com.paulo.rest.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {

  private final CalculatorService calculatorService;

  @Autowired
  public CalculatorController(final CalculatorService calculatorService) {
    this.calculatorService = calculatorService;
  }

  @PostMapping("/operation")
  public Long operation(@RequestBody Operator operator) {
    return calculatorService.performOperation(operator);
  }

  @GetMapping("/operation/{processId}")
  public Operator operation(@PathVariable Long processId) {
    return calculatorService.obtainOperation(processId);
  }




}
